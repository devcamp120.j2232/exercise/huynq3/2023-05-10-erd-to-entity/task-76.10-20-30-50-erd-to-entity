package com.devcamp.pizza365.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.repository.ICustomerRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CustomerController {

	@Autowired
	ICustomerRepository customerRepository;

	@GetMapping("/customers/last-name/{lastName}")
	public ResponseEntity<List<Customer>> getCustomersByLastNameLike(@PathVariable String lastName) {
		try {
			List<Customer> vCustomers = new ArrayList<Customer>();
			customerRepository.findByLastNameLike(lastName).forEach(vCustomers::add);
			return new ResponseEntity<>(vCustomers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/customers/first-name/{firstName}")
	public ResponseEntity<List<Customer>> getCustomersByFirstNameLike(@PathVariable String firstName) {
		try {
			List<Customer> vCustomers = new ArrayList<Customer>();
			customerRepository.findByFirstNameLike(firstName).forEach(vCustomers::add);
			return new ResponseEntity<>(vCustomers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/customers/city/{city}")
	public ResponseEntity<List<Customer>> getCustomersByCityLike(@PathVariable String city,
			@RequestParam(name = "page", required = false, defaultValue = "0") Integer page) {
		try {
			List<Customer> vCustomers = new ArrayList<Customer>();
			customerRepository.findByCityLike(city, PageRequest.of(page, 2)).forEach(vCustomers::add);
			return new ResponseEntity<>(vCustomers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/customers/state/{state}")
	public ResponseEntity<List<Customer>> getCustomersByStateLike(@PathVariable String state,
			@RequestParam(name = "page", required = false, defaultValue = "0") Integer page) {
		try {
			List<Customer> vCustomers = new ArrayList<Customer>();
			customerRepository.findByStateLike(state, PageRequest.of(page, 2)).forEach(vCustomers::add);
			return new ResponseEntity<>(vCustomers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/customers/country/{country}")
	public ResponseEntity<List<Customer>> getCustomersByCountryLike(@PathVariable String country,
			@RequestParam(name = "page", required = false, defaultValue = "0") Integer page) {
		try {
			List<Customer> vCustomers = new ArrayList<Customer>();
			customerRepository.findByCountryLike(country, PageRequest.of(page, 6)).forEach(vCustomers::add);
			return new ResponseEntity<>(vCustomers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/customers/update/{country}")
	public ResponseEntity<Object> updateCountry(@PathVariable String country) {
		try {
			int vCustomer = customerRepository.updateCountry(country);
			return new ResponseEntity<>(vCustomer, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
